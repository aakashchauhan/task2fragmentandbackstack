package com.example.task2fragmentswitch;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;

import androidx.appcompat.app.AppCompatActivity;

public class SecondClass extends AppCompatActivity {
    CheckBox checkBox;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_layout);
        checkBox = findViewById(R.id.checkBox);
    }

    public void changeFragment(View view){
            Fragment fragment;
            if (view == findViewById(R.id.btnFrg1)) {
                fragment = new FragmentOne();
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.fragment, fragment);
                if(checkBox.isChecked()) {
                    ft.addToBackStack(null);
                }
                ft.commit();
            }
            if (view == findViewById(R.id.btnFrg2)) {
                fragment = new FragmentTwo();
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.fragment, fragment);
                if(checkBox.isChecked()) {
                    ft.addToBackStack(null);
                }
                ft.commit();
            }
            if (view == findViewById(R.id.btnFrg3)) {
                fragment = new FragmentThree();
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.fragment, fragment);
                if(checkBox.isChecked()) {
                    ft.addToBackStack(null);
                }
                ft.commit();
            }
            if (view == findViewById(R.id.btnFrg4)) {
                fragment = new FragmentFour();
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.fragment, fragment);
                if(checkBox.isChecked()) {
                    ft.addToBackStack(null);
                }
                ft.commit();
            }
    }
}
